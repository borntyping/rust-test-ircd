use std::io::{TcpListener, TcpStream};
use std::io::{Acceptor, Listener};
use std::io::BufferedReader;
use std::io::File;

fn main() {
    let listener = TcpListener::bind(("127.0.0.1", 6667));

    fn handle_client(mut stream: TcpStream) {
        let mut file = BufferedReader::new(File::open(&Path::new("example.txt")));
        for line in file.lines() {
            stream.write(line.unwrap().as_bytes()).ok();
        }
    }

    // accept connections and process them, spawning a new tasks for each one
    for stream in listener.listen().incoming() {
        match stream {
            Err(e) => { println!("Connection failed: {}", e) }
            Ok(stream) => spawn(proc() {
                handle_client(stream)
            })
        }
    }
}
